# zbarcam-android-buildozer-kivy

Detectar códigos QR y EAN-13 en Android con Kivy usando zbarcam

## Preparar entorno
Se usa lo mismo que en el ejemplo de HelloWorld: https://gitlab.com/adremides/helloworld-android-buildozer-kivy

## Instalar dependencias
Las que vienen en el requirements.txt

## Copiar archivos
El directorio `kivy_garden` y todo su contenido, `buildozer.spec` y `main.py`

## Prueba ejecutando en un dispositivo Android
Compilar con `buildozer -v android debug`
Ejecutar con `buildozer android deploy run logcat`

Se puede ver el log de la app en la consola de PyCharm
